﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int array[3][4];

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            array[i][j] = j + i;
        }
    }

    printf("Елементи масиву:\n");
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            printf("%3d ", array[i][j]);
        }
        printf("\n");
    }

    int sum = 0;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            sum += array[i][j];
        }
    }
    float average = (float)sum / (3 * 4);

    printf("\nСереднє арифметичне: %.2f\n", average);

    return 0;
}