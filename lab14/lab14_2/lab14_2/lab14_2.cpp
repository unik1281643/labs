﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

#include <stdio.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    double b[5][5];
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            b[i][j] = rand() / (RAND_MAX + 1.0);
        }
    }

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%.2f ", b[i][j]);
        }
        printf("\n");
    }

    printf("\n\n\n");

    double max_element = b[0][0];
    int max_i = 0, max_j = 0;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (b[i][j] > max_element) {
                max_element = b[i][j];
                max_i = i;
                max_j = j;
            }
        }
    }

    printf("a) Максимальний елемент: %.2f, його індекси: (%d, %d)\n", max_element, max_i, max_j);

    double min_element = b[0][0];
    int min_i = 0, min_j = 0;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if (b[i][j] < min_element) {
                min_element = b[i][j];
                min_i = i;
                min_j = j;
            }
        }
    }

    printf("b) Мінімальний елемент: %.2f, його індекси: (%d, %d)\n", min_element, min_i, min_j);

    for (int i = 0; i < 5; i++) {
        double sum = 0;
        for (int j = 0; j < 5; j++) {
            sum += b[i][j];
        }
        double average = sum / 5;
        printf("c) Середнє значення рядка %d: %.2f\n", i, average);
    }

    double product_diagonal = 1.0;
    double sum_diagonal = 0.0;

    for (int i = 0; i < 5; i++) {
        product_diagonal *= b[i][i];
        sum_diagonal += b[i][i];
    }

    printf("d) Добуток елементів головної діагоналі: %.2f\n", product_diagonal);
    printf("e) Сума елементів головної діагоналі: %.2f\n", sum_diagonal);

    double sum_below_diagonal = 0.0;

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < i; j++) {
            sum_below_diagonal += b[i][j];
        }
    }

    printf("f) Сума елементів під головною діагоналлю: %.2f\n", sum_below_diagonal);

    return 0;
}