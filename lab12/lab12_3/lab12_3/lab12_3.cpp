﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

void cycle_array(int* A, int N) {
    int temp = A[N - 1];
    for (int i = N - 1; i > 0; i--) {
        A[i] = A[i - 1];
    }

    A[0] = temp;
}

int main() {
    int A[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
    int N = sizeof(A) / sizeof(A[0]);

    cycle_array(A, N);

    for (int i = 0; i < N; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");

    return 0;
}