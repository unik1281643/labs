﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

int main() {
    int array[] = { 3,2,4,3,5 };

    int maxmin = array[0];
    for (int i = 1; i < 5; i++) {
        int left = array[i - 1];
        int right = array[i + 1];
        if (array[i] < maxmin && array[i] < left && array[i] < right) {
            maxmin = array[i];
        }
    }

    printf("Max min: %d\n", maxmin);

    return 0;
}