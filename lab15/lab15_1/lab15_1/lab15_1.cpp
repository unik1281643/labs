﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

void sortRow(int row[], int size) {
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (row[j] > row[j + 1]) {
                int temp = row[j];
                row[j] = row[j + 1];
                row[j + 1] = temp;
            }
        }
    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    const int rows = 4;
    const int cols = 4;

    int matrix[rows][cols] = {
        {5, 2, 8, 1},
        {9, 4, 6, 3},
        {7, 0, 2, 5},
        {3, 1, 8, 6}
        

    };

    for (int i = 0; i < rows; i += 2) {
        sortRow(matrix[i], cols);
    }

    printf("Відсортована матриця:\n");
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }

    return 0;
}