﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

#define ROWS 5
#define COLS 8

void moveNegativeElements(int matrix[ROWS][COLS]) {
    for (int i = 0; i < ROWS; i += 2) {
        for (int j = 1; j < COLS; j++) {
            if (matrix[i][j] < 0) {
                int temp = matrix[i][j];
                int k = j;
                while (k > 0 && matrix[i][k - 1] >= 0) {
                    matrix[i][k] = matrix[i][k - 1];
                    k--;
                }
                matrix[i][k] = temp;
            }
        }
    }
}

void printMatrix(int matrix[ROWS][COLS]) {
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int matrix[ROWS][COLS] = {
        {1, -2, 3, -4, 2, 4, 6, -2},
        {5, 6, -7, 8, 2, 1, -2, 4},
        {9, -10, 11, 12, 24, -2, 2, 1},
        {5, -9, 5, 3, 1, -2 , -1, 0},
        {4, -1, 2, 2, 2, -3 , 1, -5}
    };

    printf("Початкова матриця:\n");
    printMatrix(matrix);

    moveNegativeElements(matrix);

    printf("\nМатриця після переміщення:\n");
    printMatrix(matrix);

    return 0;
}