﻿#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#include <stdio.h> 
#include <math.h> 
#include <windows.h> 


int main() {
	double x, y;
	printf("x: ");
	scanf("%lf", &x);
	printf("y: ");
	scanf("%lf", &y);
	double f1 = exp(x);
	if (y >= x + 1 && y >= -x + 1 || y <= x + 1 && y >= 0 && x <= 0 || y <= -x + 1 && y <= 0 && x >= 0) {
		printf("true\n");
	}
	else {
		printf("false\n");
	}
	return 0;
}
