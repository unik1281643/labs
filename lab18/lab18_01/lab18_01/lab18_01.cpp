﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>


int find_min(int array[], int n) {
    if (n == 1)
        return array[0];

    int rest_min = find_min(array, n - 1);
    if (rest_min < array[n - 1])
        return rest_min;
    else
        return array[n - 1];
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int arr[] = { 8, 3, 2, 7, 5, 1, 9, 4, 6, 2 };
    int size = sizeof(arr) / sizeof(arr[0]);
    int min_el = find_min(arr, size);

    printf("Мін. елем. масиву: %d\n", min_el);
    return 0;
}