﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>


int find_max(int arr[], int low, int high) {

    if (low == high) {
        return arr[low];
    }

    int mid = (low + high) / 2;
    int left_max = find_max(arr, low, mid);
    int right_max = find_max(arr, mid + 1, high);
    return (left_max > right_max) ? left_max : right_max;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int arr[] = { 8, 3, 2, 7, 5, 1, 9, 4, 6, 2 };
   
    int size = sizeof(arr) / sizeof(arr[0]);
    int max_el = find_max(arr, 0, size - 1);

    printf("Макс. елем. масиву: %d\n", max_el);
    return 0;
}