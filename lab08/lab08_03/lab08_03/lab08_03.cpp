﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int s, x1 = 4;
	double fx = 0;
	double fx2 = 0;
	double x2 = 2;
	printf("for(1) or while(2): ");
	scanf("%d", &s);

	switch (s)
	{
	case 1:
	   for (int x1 = 4; x1 <= 5; x1++) {
	        fx += x1 * sqrt(x1);
	   }
	   printf("%lf\n", fx);
	
		for (double x2 = 2; x2 <= 3.4; x2+=0.2) {
		        fx2 += 1 + exp(x2);
		} 
		printf("%lf\n", fx2);
		break;

	case 2:
		while (x1 <= 5) {
			fx += x1 * sqrt(x1);
			x1 += 1;
		}
		printf("%lf\n", fx);

		while (x2 <= 3.4) {
			fx2 += 1 + exp(x2);
			x2 += 0.2;
		}
		printf("%lf\n", fx2);
		break;

	default:
		printf("print number");
		break;
	}
}
