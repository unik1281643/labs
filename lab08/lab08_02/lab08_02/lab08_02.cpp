﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main(){
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
		int n, s, i = 1;
		
		double sum = 0;
		printf("for(1) or while(2): ");
		scanf("%d", &s);

		switch (s)
		{
		case 1:
			printf("n: ");
			scanf("%d", &n);
			for (int i = 1; i <= n; i++) {
				sum += pow(-1, i) / (2.0 * i + 1);
			}
			printf("%lf", sum);
			break;

		case 2:
			printf("n: ");
			scanf("%d", &n);
			while (i <= n) {
				sum += pow(-1, i) / (2.0 * i + 1);
				i += 1;
			}
			printf("%lf", sum);

			break;

		default:
			printf("print number");
			break;
		}
}
