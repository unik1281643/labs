﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <windows.h> 

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	double sum = 0;
	int i = 1, j;
    while (i <= 50) {
        j = 1;
        while (j <= 10) {
            sum += 1.0 / (i + j);
            j++;
        }
        i++;
    }
    printf("sum = %f\n", sum);
    return 0;
}