﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

int sum_matrix(int array[], int size);
int max_matrix(const int array[], int size);
int min_matrix(const int array[], int size);
int multiply(int array[], int size);

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    const int size = 9;
    int array[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    printf("\nЗавдання 2а: %d", sum_matrix(array, size));

    printf("\nЗавдання 2b: %d", max_matrix(array, size));

    printf("\nЗавдання 2c: %d", min_matrix(array, size));

    printf("\nЗавдання 2d: %d", multiply(array, size));

    return 0;
}

int sum_matrix(int array[], int size)
{
    int sum = 0;
    for (int i = 0; i < size; i++)
        sum += array[i];
    
    return sum;
}

int max_matrix(const int array[], int size)
{
    int max = -10000;

    for (int i = 0; i < size; i++)
        if (array[i] > max) max = array[i];

    return max;
}

int min_matrix(const int array[], int size)
{
    int min = 10000;

    for (int i = 0; i < size; i++)
        if (array[i] < min) min = array[i];

    return min;
}

int multiply(int array[], int size)
{
    if (size <= 0)
        return 1;

    return array[size - 1] * multiply(array, size - 1);
}
