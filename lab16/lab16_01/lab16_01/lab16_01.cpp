﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

float parallelogram(float a, float b, float alpha) {
	return a * b * sin(alpha);
}


int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	float a;
	float b;
	float alpha;

	printf("Ведіть довжину сторони a:\n->");
	scanf("%e", &a);
	printf("Ведіть довжину сторони в:\n->");
	scanf("%e", &b);
	printf("Кут між ними: \n->");
	scanf("%e", &alpha);

	float area = parallelogram(a, b, alpha);
	
	if (area < 0)
		area *= -1;

	printf("Площа паралелограма  = %.2f\n", area);
	return 0;
}
