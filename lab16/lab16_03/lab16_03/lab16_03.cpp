﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

int rev_num(int number) {

    int rev_numb = 0;
    while (number > 0) {
        rev_numb *= 10;
        rev_numb += number % 10;
        number /= 10;
    }

    return rev_numb;
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    int number;
    printf("Введіть любе число:");
    scanf("%d", &number);
   

    int rev_numb = rev_num(number);

    printf("Перевернуте число: %d\n", rev_numb);

    return 0;
}
