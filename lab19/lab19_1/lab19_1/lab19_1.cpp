﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(NULL));
    int *p;
    int x = 18, y = -9,
    m[] = {0, 1, 2, 3, 4, 5};
    p = &y;

    printf("p: %d\n", *p);
    x = *p;

    printf("x: %d\n", x);
    y += 7;

    printf("p: %d\n", *p);
    *p += 5;

    printf("y: %d\n", y);

    return 0;
}