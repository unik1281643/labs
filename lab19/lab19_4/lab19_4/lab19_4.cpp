﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <stdio.h>

void swapElements(int* array, int size) {
    for (int i = 0; i < size - 1; i += 2) {
        int* even = &array[i];
        int* odd = &array[i + 1];
        int temp = *even;
        *even = *odd;
        *odd = temp;
    }
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    const int size = 5;
    int array[size];
    printf("Введіть елементи масиву(5 цифр через Enter):\n");
    for (int i = 0; i < size; i++)
        scanf("%d", &array[i]);
    

    printf("Початковий масив: ");
    for (int i = 0; i < size; i++)
        printf("%d ", array[i]);
    

    swapElements(array, size);

    printf("\nЗмінений масив: ");
    for (int i = 0; i < size; i++)
        printf("%d ", array[i]);
    
    return 0;
}
