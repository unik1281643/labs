﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 
#include <time.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    const int size = 9;
    int arr[] = {1,2,3,4,5,6,7,8,9};

    printf("Розмір : %lu bytes\n", sizeof(arr));
    printf("Елементів: %lu\n", sizeof(arr) / sizeof(int));
    printf("Перший: %p\n", &arr[0]);
    printf("Останій: %p\n", &arr[size-1]);

    for (int i = 0; i < size/2; i++)
    {
        int temp = arr[size - i - 1];
        arr[size - i - 1] = arr[i];
        arr[i] = temp;
    }

    for (int i = 0; i < size; i++)
        printf("%d ", arr[i]);
    
    printf("\n");
	return 0;
}