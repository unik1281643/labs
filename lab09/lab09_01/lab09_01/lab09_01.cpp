﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
    int pos = 0;
    int neg = 0;
    int number;

    printf("Рахуємо кількість додатних та від'ємних чисел.\n Щоб закінчити рахувати напиши 0.\n");

    while (1) {
        printf("-> ");
        scanf("%d", &number);

        if (number == 0) {
            break;
        }
        else if (number > 0) {
            pos+=1; 
        }
        else {
            neg+=1; 
        }
    }

    printf("Кількість додатних чисел: %d\n", pos);
    printf("Кількість від'ємних чисел: %d\n", neg);

return 0;
}