﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int second;

    printf("Введіть час в секундах:");
    scanf("%d", &second);

    printf("Таймер на %d сек. \n", second);

    Sleep(second * 1000);  

    printf("\n *тут повиний бути сигнал*\a\n");

    return 0;
}