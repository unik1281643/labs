﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int number, ten = 0, five = 0;

    printf("Напиши любих 10 чисел.\n");

    for (int i = 0; i < 10;i++) {
        printf("Число %d: ", i + 1);
        scanf("%d", &number);

        if (number > 10) {
            ten++;
        }
        else if (number > 5) {
            five++;
        }
    }
    
    if (ten > 4) {
        printf("КАРАУЛ!");
    }
    else {
        printf("\n\nЧисла більше 10: %d\n", ten);
        printf("Числа більше 5: %d\n", five);
    }
    return 0;
}