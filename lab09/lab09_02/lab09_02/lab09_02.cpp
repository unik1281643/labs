﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h> 
#include <windows.h> 

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int number, sum, first, last;

    printf("Введіть трицифрове число: ");
    scanf("%d", &number);

    if (number >= 100 && number <= 999) {
         first = number / 100;
         last = number % 10;

         sum = first + last;

        printf("Сума крайніх цифр числа %d дорівнює %d\n", number, sum);
    }
    else {
        printf("Число має бути від 100 до 999\n");
    }
    return 0;
}